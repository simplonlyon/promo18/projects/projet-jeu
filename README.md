# Projet Jeu

L'objectif de ce projet est de réaliser un petit jeu via de la POO qui sera jouable en ligne de commande (par exemple)

## Organisation
1. Créer un gitlab dans lequel vous mettrez le jeu
2. Initialiser un projet Java maven et le lier à votre gitlab
3. Choisir un jeu (exemple de jeu plus bas pour les gens qui n'ont pas d'idée)
4. Créer un fichier Readme.md à la racine de votre projet, indiquer le jeu que vous avez choisi, et les règles que vous allez coder. Attention, ce fichier est au format Markdown, regardez la syntaxe pour formater correctement votre Readme !
5. Commencer à refléchir aux classes et méthode dont on aura besoin pour le jeu, pour ça on peut essayer de se demander "quelles sont les actions possibles sur mon jeu ?"
6. On peut essayer les actions en appelant directement les méthodes dans le main avant de rajouter l'interactivité avec la ligne de commande (ou Swing ou ce que vous voulez en vrai, mais l'objectif principale c'est que le jeu fonctionne, qu'il soit interactif ou non)


## Idées de jeux
* Wordle
* Un morpion
* Un puissance 4
* Un sudoku ?
* Un memory
* Une bataille navale
* Un tamagochi
* Un mini rpg (gestion de personnage, d'équipement, d'interaction avec d'autres personnages, de placement sur une grille, etc.)
